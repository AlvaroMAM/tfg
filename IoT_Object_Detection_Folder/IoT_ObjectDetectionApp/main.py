import os
import YoloClass
import ImageClass
import requests
import time
import json
import base64
import numpy as np
from kafka import KafkaConsumer
from MyExceptionClass import MyException
from cv2 import cv2
from datetime import datetime
#KAFKACONSUMER PARAMETERS
#BOOTSTRAP_SERVER_1='127.0.0.1:9094' #FOR TESTING
#BOOTSTRAP_SERVER_2='127.0.0.1:9095' #FOR TESTING
BOOTSTRAP_SERVER_1='kafka1:9092' #ON DOCKER
BOOTSTRAP_SERVER_2='kafka2:9092' #ON DOCKER
AUTO_OFFSET_RESET="latest"
ENABLE_AUTO_COMMIT = True
#ATTRIBUTES
LESHAN_TIME='/3/0/13' #TESTING
LESHAN_RESOURCE='/39574/0/'
LESHAN_RESOURCE_IMAGE_NEW=LESHAN_RESOURCE+'27360'
LESHAN_RESOURCE_IMAGE_NAME=LESHAN_RESOURCE+'27362'
LESHAN_RESOURCE_IMAGE_DATE=LESHAN_RESOURCE+'27363'
LESHAN_RESOURCE_IMAGE_BYTES=LESHAN_RESOURCE+'27366'
LESHAN_RESOURCE_IMAGE_REQUESTS=LESHAN_RESOURCE+'27364'
#OPERATIONS
READ='READ'
OBSERVE='OBSERVE'
EXECUTE='EXECUTE'
DELETE_OBSERVATION='DELETE_OBSERVATION'
#URL-SHADOWAPPLICATIONS
#URL_SHADOWAPPLICATIONS='http://127.0.0.1:8003/' #FOR TESTING
URL_SHADOWAPPLICATIONS='http://iotshadowapplications:80/' #ON DOCKER
URL_SHADOWAPPLICATIONS_INTEREST=URL_SHADOWAPPLICATIONS+'interest/'
URL_SHADOWAPPLICATIONS_ACTION=URL_SHADOWAPPLICATIONS+'action/'
#EXTRA-DATA
APP_NAME='ObjectDetectionApp'
WAITING_TIME=5
REQUESTS_SUCCESS_CODE=200
REQUESTS_NOT_FOUD_CODE=404


def interest(operation,resource,info):
    """
    The application requests the intention of reading a resource in the future.
    If the resource is not available, it will poll the resource until it is available
    Return: True if the resource is available
            False if it was a bad request
    """
    whileCondition= False
    resourceAvailable=False
    while(not(whileCondition)):
        interestRequest= requests.post(url=URL_SHADOWAPPLICATIONS_INTEREST,data=info)
        if(interestRequest.status_code == REQUESTS_SUCCESS_CODE):
            resourceAvailable= True
            print("Interest of operation "+operation+ " has been successed")
            whileCondition= True
        elif(interestRequest.status_code== REQUESTS_NOT_FOUD_CODE):
            print("Waiting for resource")
            time.sleep(WAITING_TIME)
        else:
            whileCondition=True
    return resourceAvailable
            
def operation(operation,resource):
    """
    1. The applications shows interest for a resource of a physical device
    2. If '1.' is successed, then the application will realize an action on that resource. 
    Returns: Data from KafkaProducer
             Produces an execption       
    """
    info = {
        'app_name': APP_NAME,
        'resource_accessing': resource,
        'operation': operation
    }
    interestSuccessed = interest(operation,resource,info)
    if(interestSuccessed):
        actionRequest=requests.post(url=URL_SHADOWAPPLICATIONS_ACTION,data=info)
        if(actionRequest.status_code==REQUESTS_SUCCESS_CODE):
            data=""
            kafka_topic = json.loads(actionRequest.text)['kafka_topic']
            consumer = KafkaConsumer(bootstrap_servers=[BOOTSTRAP_SERVER_1,BOOTSTRAP_SERVER_2], auto_offset_reset=AUTO_OFFSET_RESET,value_deserializer=lambda m: json.loads(m.decode('utf-8')))
            consumer.subscribe(kafka_topic)
            print("Consumer is subscribed", flush=True)
            if(operation==READ):
                try:
                    for message in consumer:
                        value = message.value
                        print(value)
                        data = value['data']
                        break
                    return data
                except KeyboardInterrupt as e:
                    print(e)
                    print("Detected Keyboard Interrupt. Cancelling.")
                    pass
                finally:
                    consumer.close()
            elif (operation==OBSERVE): #OBSERVATION
                try:
                    for message in consumer:
                        value = message.value
                        print(value)
                        if(len(value['value'])>0): 
                            data += value['value']
                        else:
                            #We have read the bytes
                            break    
                    return data
                except KeyboardInterrupt as e:
                    print(e)
                    print("Detected Keyboard Interrupt. Cancelling.")
                    pass
                finally:
                    consumer.close()
            else:
                try:
                    for message in consumer:
                        value = message.value
                        data = value['success']
                        break
                    return data
                except KeyboardInterrupt as e:
                    print(e)
                    print("Detected Keyboard Interrupt. Cancelling.")
                    pass
                finally:
                    consumer.close()
        else:
            raise MyException("Something was wrong during action request of operation "+operation)


    else:
        raise MyException("Something was wrong during interest request of operation "+operation)
    


def main():
    try:
        yoloObject = YoloClass.loadingYolo()
        auxImage=ImageClass.Aux("","","")
        while(True):
            success = operation(EXECUTE,LESHAN_RESOURCE_IMAGE_NEW)
            if(success):
                print("New Picture loaded")   
            else:
                raise MyException("Error during execute action")
            #READ Image Name
            name = operation(READ,LESHAN_RESOURCE_IMAGE_NAME)
            auxImage.setNameAux(name)
            #READ IMAGE Date
            date = operation(READ,LESHAN_RESOURCE_IMAGE_DATE)
            auxImage.setDateAux(date)
            #OBSERVE BYTES
            imageBytes = operation(OBSERVE,LESHAN_RESOURCE_IMAGE_BYTES)
            auxImage.setBytesAux(imageBytes)
            success = operation(DELETE_OBSERVATION,LESHAN_RESOURCE_IMAGE_BYTES)
            if(success):
                print("DELETE SUCCESS")
                #ANALYSIS START
                bytesAuxImage = auxImage.getBytesAux()
                decodedBytes = base64.b64decode(bytesAuxImage)
                array = np.frombuffer(decodedBytes,np.uint8)
                img = cv2.imdecode(array,cv2.IMREAD_UNCHANGED)
                detectionImage= ImageClass.readingImage(auxImage.getNameAux(),auxImage.getDateAux(),img)
                class_ids, boxes, indexes = YoloClass.identifyObjects(detectionImage,yoloObject)
                YoloClass.drawObject(detectionImage.getImage(),class_ids,boxes,indexes,yoloObject.getClasses(),yoloObject.getColors())
                response = ImageClass.saveImageDB(detectionImage)
                if (response.status_code == REQUESTS_SUCCESS_CODE):
                    print("Image saved on bd succesfully")
                else:
                    raise MyException("Something was wrong during the saved of the image on BD")
                #ANALYSIS FINISHED
            else:
                raise MyException("Error deleting observation")
    except Exception as e:
        print(e)
    finally:
        pass

if __name__ == "__main__":
    main()