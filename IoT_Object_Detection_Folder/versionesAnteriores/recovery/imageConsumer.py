import copy
import os
import numpy as np
from kafka import KafkaConsumer
import json
from cv2 import cv2
import json

BOOTSTRAP_SERVERS = 'localhost:9092'
ENCODE = 'utf-8'
TOPIC = 'pruebaImagen'
AUTO_OFFSET_RESET= 'earliest'
ENABLE_AUTO_COMMIT = True
AUTO_COMMIT_INTERVAL_MS= 1000
GROUP_ID= 'my-group'

consumer= KafkaConsumer(TOPIC, bootstrap_servers=[BOOTSTRAP_SERVERS], auto_offset_reset=AUTO_OFFSET_RESET, enable_auto_commit=ENABLE_AUTO_COMMIT, group_id=GROUP_ID, value_deserializer=None)

for c in consumer:
    recievedImage = c.value
    array = np.frombuffer(recievedImage,np.uint8)
    img= cv2.imdecode(array,cv2.IMREAD_COLOR)
    
    print(img)
   
    