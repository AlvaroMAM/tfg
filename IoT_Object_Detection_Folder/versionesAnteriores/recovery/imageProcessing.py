"""
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
"""
import copy
import os
import numpy as np
import auxiliaFunctions as af
from auxiliarClasses import MyException, YoloClass, Image
from kafka import KafkaConsumer
from json import loads
from cv2 import cv2
from datetime import datetime

#Guardar imagenbd
#BOOTSTRAP_SERVERS_BD = 'localhost:9092'
#TOPIC_BD=
BOOTSTRAP_SERVERS = 'localhost:9092'
ENCODE = 'utf-8'
TOPIC = 'pruebaImagen'
AUTO_OFFSET_RESET= 'earliest'
GROUP_ID= 'my-group'
IMAGE_NAME = 'IMG_'
YOLO_WEIGHTS="yolov3.weights"
COCO_NAMES="cocoSpanish.names"
YOLO="yolov3.cfg"
EXTENSION_IMAGE="_processed.jpg"
POLYGON_NUMBER=6 
EXIT_KEY= 27
POLYGON_ACCURACY= 0.009
AUTO_COMMIT_INTERVAL_MS= 1000
ENABLE_AUTO_COMMIT = True
VALUE_SERIALIZER = None



def saveImageDB(producerDB,originalImage,detectionImage):
    #Modify the image name
    #Posible paralelización con hebras y variable común productor
    _,original = cv2.imencode('.jpg',originalImage)
    _, detection = cv2.imencode('.jpg',detectionImage)
    bytesImageOriginal = original.tobytes()
    bytesImageObject = detection.tobytes()
    #dataOriginal = {'imageArray': bytesImageOriginal}
    #dataObject = {'imageArray': bytesImageObject}
    producerDB.send(TOPIC, value=bytesImageOriginal)
    producerDB.send(TOPIC, value=bytesImageObject)

    #producerDB.send(TOPIC, value=dataOriginal)
    #producerDB.send(TOPIC, value=dataObject)
    return


def drawObject(image, class_ids, boxes, indexes, classes, colors):
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[i]
            cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
            cv2.putText(image, label, (x, y + 30), font, 3, color, 3)


def identifyObjects(imageObject,yoloObject):
    blob = cv2.dnn.blobFromImage(imageObject.getImage(), 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    yoloObject.getNet().setInput(blob)
    outs = yoloObject.getNet().forward(yoloObject.getOutputLayers())
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                # Object detected
                center_x = int(detection[0] * imageObject.getWidth())
                center_y = int(detection[1] * imageObject.getHeight())
                w = int(detection[2] * imageObject.getWidth())
                h = int(detection[3] * imageObject.getHeight())
                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)
                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    return class_ids, boxes, indexes

def readingImage(img):
    #Creates an Image object
    now = datetime.now().strftime('%Y%m%d-%H%M%S%f')
    image = Image(IMAGE_NAME+now,img)
    if image != None:
        print("Image load correctly")
        return image 
    else:
        raise MyException("Image can not be read")
    
def loadingYolo():
    net = cv2.dnn.readNet(YOLO_WEIGHTS,YOLO)
    classes = []
    with open(COCO_NAMES,"r") as f:
        classes =[line.strip() for line in f.readlines()]
    layer_names = net.getLayerNames()
    output_layers =[layer_names[i[0]-1] for i in net.getUnconnectedOutLayers()]
    colors = np.random.uniform(0, 255, size=(len(classes), 3))
    yoloObject = YoloClass(net, classes, layer_names, output_layers, colors)
    return yoloObject

def main():
   try:
    yoloObject = loadingYolo()   
    consumer= KafkaConsumer(TOPIC, bootstrap_servers=[BOOTSTRAP_SERVERS], auto_offset_reset=AUTO_OFFSET_RESET, enable_auto_commit=ENABLE_AUTO_COMMIT, group_id=GROUP_ID, value_deserializer=VALUE_SERIALIZER)
    #producerDB = KafkaProducer(bootstrap_servers=[BOOTSTRAP_SERVERS], value_serializer=None)
    for c in consumer:
        recievedImage = c.value
        array = np.frombuffer(recievedImage,np.uint8)
        img= cv2.imdecode(array,cv2.IMREAD_COLOR)
        originalImage = readingImage(img)
        #Creates a copy of the original
        detectionImage = copy.deepcopy(originalImage)
        #Add the sufix PROCESSED_IMAGE 
        if(detectionImage != None):
            print("The copy of the image has been made successfully")

        class_ids, boxes, indexes = identifyObjects(detectionImage,yoloObject)
        drawObject(detectionImage.getImage(),class_ids,boxes,indexes,yoloObject.getClasses(),yoloObject.getColors())
        af.saveImage(detectionImage)
        #saveImageDB(producerDB,originalImage,detectionImage)

   except Exception as myE:
       print(myE)
       
if __name__ == "__main__":
    main()    