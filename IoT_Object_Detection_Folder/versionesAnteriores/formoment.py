#OBTENER TOKEN DATABASE
    
    
    

        
       
            
            
            if request.status_code == STATUS_CODE_SUCCESS:
            
                consumer= KafkaConsumer(bootstrap_servers=[BOOTSTRAP_SERVER_1,BOOTSTRAP_SERVER_2], auto_offset_reset=AUTO_OFFSET_RESET,value_deserializer=VALUE_SERIALIZER)
                kafka_topic = json.loads(request.text)['kafka-topic']
                consumer.subscribe([kafka_topic])
                
                #START OBSERVATION
                while(True):
                    for c in consumer:
                        message = c.value
                        if(auxImage.isEmpty()):
                            auxImage.setNameAux(message['Image Name'])
                            auxImage.setDateAux(message['Time Stamp'])
                            auxImage.setBytesAux(message['Bytes'])
                        else :
                            if(message['Image Name'] != auxImage.getNameAux()):
                                #Se ha producido un cambio de imagen, tengo que modificar el valor de auxImagen
                                auxImage.setNameAux(message['Image Name'])
                                auxImage.setDateAux(message['Time Stamp'])
                                auxImage.setBytesAux(message['Bytes'])
                            else:
                                #No he cambiado de imagen
                                if(len(message['Bytes'])!=0):
                                    #comprobar si falla el número de peticiones realizadas vs totales
                                    auxImage.concBytes(message['Bytes'])
                                else:
                                    #I have finished reading the image
                                    break

                    request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(CANCEL_OBSERVATION), headers=headers)
                    if request.status_code == STATUS_CODE_SUCCESS:
                        #OBSERVE STOPPED
                        #START ANALYSIS
                        bytesAuxImage = auxImage.getBytesAux()
                        decodedBytes = base64.b64decode(bytesAuxImage)
                        array = np.frombuffer(decodedBytes,np.uint8)
                        img = cv2.imdecode(array,cv2.IMREAD_COLOR)
                        detectionImage= ImageClass.readingImage(img,auxImage.getNameAux(),auxImage.getDateAux())
                        #originalImage = ImageClass.readingImage(img,auxImage.getNameAux(),auxImage.getDateAux())
                        #Creates a copy of the original
                        #detectionImage = copy.deepcopy(originalImage)
                        #Add the sufix PROCESSED_IMAGE 
                        if(detectionImage != None):
                            print("The copy of the image has been made successfully")

                        class_ids, boxes, indexes = YoloClass.identifyObjects(detectionImage,yoloObject)
                        YoloClass.drawObject(detectionImage.getImage(),class_ids,boxes,indexes,yoloObject.getClasses(),yoloObject.getColors())
                        #ImageClass.saveImage(detectionImage)
                        response = ImageClass.saveImageDB(detectionImage)
                        if response.status_code == 200:
                            print("Image saved on bd succesfully")
                        else:
                            print("Something was wrong")
                        #ANALYSIS FINISHED

                        request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(EXECUTE), headers=headers)
                        if request.status_code == STATUS_CODE_SUCCESS:
                            #TAKE NEW PHOTO
                            request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(OBSERVATION), headers=headers)
                            #STARTING OBSERVATION
                
            else:
                raise MyException("Error during action in shadow app")
            
        else:
            raise MyException("Error doing interest")