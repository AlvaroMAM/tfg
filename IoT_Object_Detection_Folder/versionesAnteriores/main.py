"""
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
"""
import copy
import os
import numpy as np
import YoloClass
import ImageClass
import requests
import json
import time
import base64
from MyExceptionClass import MyException
from kafka import KafkaConsumer, KafkaProducer
from json import loads
from cv2 import cv2
from datetime import datetime

#URLS
URL_BD = 'http://mongoapi:80/'  # on docker swarm
URL_BD_GENERATE_TOKEN='generateToken/'
#URL_BD = 'http://127.0.0.1:8081/'  # on local for tests

URL_SHADOW_APPLICATION_INTEREST= "http://127.0.0.1:8003/interest"
URL_SHADOW_APPLICATION_ACTION="http://127.0.0.1:8003/action"
#KAFKA_CONSUMER
BOOTSTRAP_SERVER_1 = '127.0.0.1:9094'
BOOTSTRAP_SERVER_2 = '127.0.0.1:9095'
ENCODE = 'utf-8'
AUTO_OFFSET_RESET= 'latest'
AUTO_COMMIT_INTERVAL_MS= 1000
ENABLE_AUTO_COMMIT = True
VALUE_SERIALIZER = lambda m: json.loads(m.decode(ENCODE))
GROUP_ID= 'my-group'
#DATA
AUTHORIZATION = 'Authorization'
APP_NAME= 'Image Object Detection'
DEVICE = '/39574/39574'
DEVICE_RESOURCE_NEW_IMAGE = '/39574/39574/27360'
DEVICE_RESOURCE_IMAGE_NAME = '/39574/39574/27362'
DEVICE_RESOURCE_IMAGE_DATE = '/39574/39574/27363'
DEVICE_RESOURCE_IMAGE_BYTES = '/39574/39574/27366'
OBSERVATION = 'OBSERVE'
READ = 'READ'
CANCEL_OBSERVATION = 'DELETE_OBSERVATION'
EXECUTE= 'EXECUTE'
#SUCCESS 
STATUS_CODE_SUCCESS = 200

def interest(resource,operation,headers):
    #El recurso puede o no estar disponible, comprobar cada 10 segundos si está registrado
            
    resourceRegistered=False
    while(not(resourceRegistered)):
        request = requests.post(url=URL_SHADOW_APPLICATION_INTEREST,data=changeInfo(operation,resource), headers=headers)
        if request.status_code == STATUS_CODE_SUCCESS:
            resourceRegistered = True
        time.sleep(10)
    return resourceRegistered


def operationCancelAndExecute(resource,operation,headers):
    resourceRegistered = interest(resource,operation,headers)
     #Hago acción sobre el recurso que quiero
    resourceAvailable=False
    if (resourceRegistered):

        while(not(resourceAvailable)):

            request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(operation,resource), headers=headers)
            if request.status_code == STATUS_CODE_SUCCESS:
                resourceAvailable = True
            time.sleep(10)
        return resourceAvailable
    else:
        raise MyException("Error during interest in CANCEL_OBSERVATION from resource "+resource)


def operationObserve(resource,operation,headers):
    resourceRegistered = interest(resource,operation,headers)
     #Hago acción sobre el recurso que quiero
    resourceAvailable=False
    if (resourceRegistered):

        while(not(resourceAvailable)):

            request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(operation,resource), headers=headers)
            if request.status_code == STATUS_CODE_SUCCESS:
                kafka_topic = json.loads(request.text)['kafka-topic']
                resourceAvailable = True
            time.sleep(10)

        consumer= KafkaConsumer(bootstrap_servers=[BOOTSTRAP_SERVER_1,BOOTSTRAP_SERVER_2], auto_offset_reset=AUTO_OFFSET_RESET,value_deserializer=VALUE_SERIALIZER)    
        consumer.subscribe([kafka_topic])
        for c in consumer:
            #bytesImages = c.value
            bytesImages = c.value["value"]
            if(len(bytesImages)>0):
                message+=bytesImages
            else:
                #he recibido toda la informacion
                break
        return message
    else:
         raise MyException("Error during interest in OBSERVATION from resource "+resource)


def operationRead(resource,operation,headers):
    #En caso de que message no sea lo que estoy pensando en estos momentos 
    #probar con  
    # auxImage.setNameAux(message['Image Name'])
    #auxImage.setDateAux(message['Time Stamp'])
    #auxImage.setBytesAux(message['Bytes'])

   
    resourceRegistered = interest(resource,operation,headers)
     #Hago acción sobre el recurso que quiero
    resourceAvailable=False
    if (resourceRegistered):

        while(not(resourceAvailable)):

            request = requests.post(url=URL_SHADOW_APPLICATION_ACTION,data=changeInfo(operation,resource), headers=headers)
            if request.status_code == STATUS_CODE_SUCCESS:
                kafka_topic = json.loads(request.text)['kafka-topic']
                resourceAvailable = True
            time.sleep(10)

        consumer= KafkaConsumer(bootstrap_servers=[BOOTSTRAP_SERVER_1,BOOTSTRAP_SERVER_2], auto_offset_reset=AUTO_OFFSET_RESET,value_deserializer=VALUE_SERIALIZER)    
        consumer.subscribe([kafka_topic])
        for c in consumer:
            message = c.value
            break
            
                
        return message
    else:
        raise MyException("Error during interest from READ resource "+resource)


def changeInfo (operation,resource):
    info = {
        'app_name': APP_NAME,
        'resource_accessing': resource,
        'operation': operation
    }

    return info

def main():
    try:
        dictDB = {'type' : 'COMPONENT'}
        requestDB = requests.post(url=URL_BD+URL_BD_GENERATE_TOKEN,data=dictDB)
        if requestDB.status_code == STATUS_CODE_SUCCESS:
            #He obtenido el token, ahora voy a mostrar interés por un recurso
            token = json.loads(requestDB.text)['token'] 
            yoloObject = YoloClass.loadingYolo() 
            headers = {AUTHORIZATION:"Token {}".format(token)}
            #NEWPHOTO
            iname = ""
            idate = ""
            ibytes = ""
            auxImage = ImageClass.Aux(iname,idate,ibytes)
            auxImage.setNameAux(operationRead(DEVICE_RESOURCE_IMAGE_NAME,READ,headers))
            auxImage.setDateAux(operationRead(DEVICE_RESOURCE_IMAGE_DATE,READ,headers))
            #bytes
            auxImage.setBytesAux(operationObserve(DEVICE_RESOURCE_IMAGE_BYTES,OBSERVATION,headers))
            #Puede que todo esto esté en un bucle
            #Cancelo la observacion
            if(operationCancelAndExecute(DEVICE_RESOURCE_IMAGE_BYTES,CANCEL_OBSERVATION,headers)):
                #OBSERVE STOPPED
                #START ANALYSIS
                bytesAuxImage = auxImage.getBytesAux()
                decodedBytes = base64.b64decode(bytesAuxImage)
                array = np.frombuffer(decodedBytes,np.uint8)

                #PUEDE QUE HAYA QUE USAR LO DE ABAJO
                #img = cv2.imdecode(array,cv2.IMREAD_UNCHANGED)

                img = cv2.imdecode(array,cv2.IMREAD_COLOR)
                detectionImage= ImageClass.readingImage(img,auxImage.getNameAux(),auxImage.getDateAux())

                #NO HACER COPIA
                #originalImage = ImageClass.readingImage(img,auxImage.getNameAux(),auxImage.getDateAux())
                #Creates a copy of the original
                #detectionImage = copy.deepcopy(originalImage)
                #Add the sufix PROCESSED_IMAGE 
                #if(detectionImage != None):
                #    print("The copy of the image has been made successfully")

                class_ids, boxes, indexes = YoloClass.identifyObjects(detectionImage,yoloObject)
                YoloClass.drawObject(detectionImage.getImage(),class_ids,boxes,indexes,yoloObject.getClasses(),yoloObject.getColors())
                #ImageClass.saveImage(detectionImage)
                response = ImageClass.saveImageDB(detectionImage)
                if response.status_code == 200:
                    print("Image saved on bd succesfully")
                else:
                    print("Something was wrong")
                #ANALYSIS FINISHED


            else:
                raise MyException("Something wrong during CANCEL_OBSERVATION "+DEVICE_RESOURCE_IMAGE_BYTES)

            newPhoto = operationCancelAndExecute(DEVICE_RESOURCE_NEW_IMAGE,EXECUTE,headers)
            print("NEW IMAGE LOADED")

          
            


            
            

        else:
            raise MyException("Error during getting token from db")

    except Exception as myE:
       print(myE)
       
if __name__ == "__main__":
    main()    