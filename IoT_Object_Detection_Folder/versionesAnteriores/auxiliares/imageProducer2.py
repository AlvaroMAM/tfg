"""
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
"""
from time import sleep
import os
import numpy as np
import base64
from cv2 import cv2
from json import dumps
from kafka import KafkaProducer

BOOTSTRAP_SERVERS = 'localhost:9092'
ENCODE = 'utf-8'
TOPIC = 'pruebaImagen'
IMAGE_ROUTE = 'person.jpg'
#IMAGE_ROUTE = 'animals.jpg'

producer = KafkaProducer(bootstrap_servers=[BOOTSTRAP_SERVERS], value_serializer=None)
image = cv2.imread(IMAGE_ROUTE)
returnValue, img = cv2.imencode('.jpg',image)
image = base64.b64encode(img)
#bytesImage = img.tobytes()


for e in range(2):
    #info = {'imageArray': img}
    producer.send(TOPIC, value=image)
    print("Image send successfuly")
    print(e)
    sleep(100)
"""for c in consumer:
                #Si
                recievedImage = c.value
                array = np.frombuffer(recievedImage,np.uint8)
                img = cv2.imdecode(array,cv2.IMREAD_COLOR)
                originalImage = ImageClass.readingImage(img)
                #Creates a copy of the original
                detectionImage = copy.deepcopy(originalImage)
                #Add the sufix PROCESSED_IMAGE 
                if(detectionImage != None):
                    print("The copy of the image has been made successfully")

                class_ids, boxes, indexes = YoloClass.identifyObjects(detectionImage,yoloObject)
                YoloClass.drawObject(detectionImage.getImage(),class_ids,boxes,indexes,yoloObject.getClasses(),yoloObject.getColors())
                ImageClass.saveImage(detectionImage)
                #Pedir
                #saveImageDB(producerDB,originalImage,detectionImage)
        """
