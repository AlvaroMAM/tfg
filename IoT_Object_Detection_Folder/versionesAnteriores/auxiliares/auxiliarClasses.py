class MyException(ValueError):
    def __init__(self,mensaje,*args):
        super(MyException,self).__init__(mensaje,*args)

class Image:
    def __init__(self,name,image):
        #os.path.splitext(name)[0] deletes the last extension of the file
        self.name = name
        self.image = image
        self.height, self.width, self.channels = image.shape
    def getImage(self):
        return self.image
    def setImage(self,newImage):
        self.image = newImage
    def getName(self):
        return self.name
    def setName(self,newName):
        self.name = newName
    def getHeight(self):
        return self.height
    def getWidth(self):
        return self.width
    def getChannels(self):
        return self.channels
    def myShow(self):
        print("My name: " + self.name)

class YoloClass:
    def __init__(self,net, classes, layer_names, output_layers, colors):
        self.net=net
        self.classes=classes
        self.layer_names= layer_names
        self.output_layers=output_layers
        self.colors=colors
    def getNet(self):
        return self.net
    def getClasses(self):
        return self.classes
    def getLayerNames(self):
        return self.layer_names
    def getOutputLayers(self):
        return self.output_layers
    def getColors(self):
        return self.colors