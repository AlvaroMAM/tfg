"""
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
"""

import numpy as np
from cv2 import cv2
YOLO_WEIGHTS="yolov3.weights"
COCO_NAMES="coco.names"
YOLO="yolov3.cfg"

class YoloClass:
    def __init__(self,net, classes, layer_names, output_layers, colors):
        self.net=net
        self.classes=classes
        self.layer_names= layer_names
        self.output_layers=output_layers
        self.colors=colors
    def getNet(self):
        return self.net
    def getClasses(self):
        return self.classes
    def getLayerNames(self):
        return self.layer_names
    def getOutputLayers(self):
        return self.output_layers
    def getColors(self):
        return self.colors
def loadingYolo():
    #This function loads yolo, and everything that its needs for processing the image, like the names of the 
    #objects that can be detected, colors for rectangles ...
    net = cv2.dnn.readNet(YOLO_WEIGHTS,YOLO)
    classes = []
    with open(COCO_NAMES,"r") as f:
        classes =[line.strip() for line in f.readlines()]
    layerNames = net.getLayerNames()
    outputLayers =[layerNames[i[0]-1] for i in net.getUnconnectedOutLayers()]
    colors = np.random.uniform(0, 255, size=(len(classes), 3))
    yoloObject = YoloClass(net, classes, layerNames, outputLayers, colors)
    return yoloObject

def identifyObjects(imageObject,yoloObject):
    #This function identifies the objects in the image
    blob = cv2.dnn.blobFromImage(imageObject.getImage(), 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    yoloObject.getNet().setInput(blob)
    outs = yoloObject.getNet().forward(yoloObject.getOutputLayers())
    ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                # Object detected
                center_x = int(detection[0] * imageObject.getWidth())
                center_y = int(detection[1] * imageObject.getHeight())
                w = int(detection[2] * imageObject.getWidth())
                h = int(detection[3] * imageObject.getHeight())
                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)
                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                ids.append(class_id)
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    return ids, boxes, indexes

def drawObject(image, class_ids, boxes, indexes, classes, colors):
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            color = colors[i]
            cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
            cv2.putText(image, label, (x, y + 30), font, 3, color, 3)