"""
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
"""
import base64
import requests
from datetime import datetime
from MyExceptionClass import MyException
from cv2 import cv2
IMAGE_NAME = 'IMG_'
EXTENSION_IMAGE="_processed.jpg"
IMAGES_APIDB_URL='http://127.0.0.1:5001/saveImage' #TESTING_LOCAL
#IMAGES_APIDB_URL='http://imagesapidb:5001/saveImage' #ON_DOCKER

class Aux:
    def __init__(self,name,idate,ibytes):
        #os.path.splitext(name)[0] deletes the last extension of the file
        self.imageName = name
        self.imageBytes = ibytes
        self.imageDate = idate
    def getNameAux(self):
        return self.imageName
    def setNameAux(self,n):
        self.imageName = n   
    def getDateAux(self):
        return self.imageDate
    def setDateAux(self,d):
        self.imageDate = d
    def getBytesAux(self):
        return self.imageBytes
    def setBytesAux(self,b):
        self.imageBytes = b       
    def concBytes(self,b):
        self.imageBytes+=b
    def isEmpty (self):
        return len(self.imageName) == 0 


class Image:
    def __init__(self,name,dateTaken,image):
        self.name = name
        self.dateTaken= dateTaken
        self.image = image
        self.height, self.width, self.channels = image.shape
    def getImage(self):
        return self.image
    def setImage(self,newImage):
        self.image = newImage
    def getName(self):
        return self.name
    def setName(self,newName):
        self.name = newName
    def getHeight(self):
        return self.height
    def getWidth(self):
        return self.width
    def getChannels(self):
        return self.channels
    def getDateTaken(self):
        return self.dateTaken
    def myShow(self):
        print("My name: " + self.name)
        return

def readingImage(name,date,img):
    #This function creates an Image object, and return it
    image = Image(name,date,img)
    if image != None:
        print("Image load correctly")
        return image 
    else:
        raise MyException("Image can not be read")

def saveImage(imageObject):
    
    imageObject.setName(imageObject.getName()+EXTENSION_IMAGE)
    saved = cv2.imwrite(imageObject.getName(),imageObject.getImage())
    if(saved):
        print("Image saved correctly")
    else:
        raise MyException("Image can not be saved")
    return

def saveImageDB(detectionImage):
    _, encodedCV2 = cv2.imencode('.jpg',detectionImage.getImage())
    encodedImage = base64.b64encode(encodedCV2)
    dataOriginal = {'imageName': detectionImage.getName(),'imageDate': detectionImage.getDateTaken() ,'imageBytes': encodedImage}
    response = requests.post(IMAGES_APIDB_URL,data=dataOriginal) #TESTING
    #response = requests.post(IMAGES_APIDB_URL,data=dataOriginal) #ON DOCKER
    return response      