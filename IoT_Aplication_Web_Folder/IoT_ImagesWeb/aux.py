import requests
DOCKER_URL ='http://imagesapidb:5001/'
TESTING_URL ='http://127.0.0.1:5001/'
class User:
    def __init__(self,name,email):
        self.userName= name
        self.userEmail=email
    def getUserName(self):
        return self.userName
    def setUserName(self,name):
        self.userName = name
    def getEmail(self):
        return self.userEmail
    def setEmail(self,email):
        self.userEmail = email
    def delete(self):
        #request = requests.delete(url=TESTING_URL+'deleteUser',data={'userEmail':self.getEmail()})
        request = requests.delete(url=DOCKER_URL+'deleteUser',data={'userEmail':self.getEmail()})
        if request.status_code==200:
            return True
        else:
            return False
    def disconnect(self):
        self.setEmail("")
        self.setUserName("")

    def loginOrRegister(self,userName,userEmail):
        #request = requests.post(TESTING_URL+'saveUser',data={'userName': userName, 'userEmail': userEmail})
        request = requests.post(DOCKER_URL+'saveUser',data={'userName': userName, 'userEmail': userEmail})
        if request.status_code == 200:
            self.setEmail(userEmail)
            self.setUserName(userName)
            return True
        else:
            return False

class Image:
    def __init__(self,i,name,date,dateProc,imageB):
        self.index = i
        self.imageName = name
        self.imageDate = date
        self.imageDateProc = dateProc
        self.imageBytes = imageB
    def getName(self):
        return self.imageName
    def getDate(self):
        return self.imageDate
    def getDateProc(self):
        return self.imageDateProc
    def getImageBytes(self):
        return self.imageBytes
    def getIndex(self):
        return self.index
    def setName(self,name):
        self.imageName = name
    def setDate(self,date):
        self.imageDate = date
    def setDateProc(self,dateProc):
        self.imageDateProc = dateProc
    def setImageBytes(self,iB):
        self.imageBytes = iB
    def setIndex(self,i):
        self.index = i
