Flask==1.1.2
Jinja2==2.11.2
numpy==1.19.0
opencv-python==4.2.0.34
pymongo==3.10.1
requests==2.21.0
Werkzeug==1.0.1