/*
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
 */

package imageDataObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;


public class MyImage {


	private final static int SUBSTRING_IMAGE_SIZE = 8154;//8188
	private int id;
	private String name;
	private Date date;
	private int size;
	private int actualRequest; //The counter of how many request the server has done to the actual image
	private String imageEncoded;
	private List<String> imageList;//List that contains the image splited
	protected String dataToSend;


	public MyImage(String path,String imageName ,int id) {
		if (path.length() == 0 || path == null) {
			throw new RuntimeException("Wrong path");
		} else {
			this.id = id;
			if (imageName.length() == 0 || imageName == null) {
				this.name = "Image_" + id;
			} else {
				this.name = imageName;
			}
			date = new Date();
			actualRequest = 0;
			dataToSend="";
			imageList = new ArrayList<String>();
			FileInputStream file;
			try {
				file = new FileInputStream(path + imageName);
				byte[] data = file.readAllBytes();
				file.close();
				// This change is made because of an error using jdk 9 and the example of leshan server,
				// the method above can be used if we have jdk 9 installed i our system
				//byte [] data = Files.readAllBytes(Paths.get(path+imageName));
				imageEncoded = Base64.getEncoder().encodeToString(data);
				fillImageList();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getActualRequest() {
		return actualRequest;
	}

	public void setActualRequest(int lastRequest) {
		this.actualRequest = lastRequest;
	}

	public int getSize() {
		return size;
	}
	private void setSize(int s) {
		this.size = s;
	}
	
	public String getImagePart() {
		if(actualRequest<size) {
			int p = actualRequest;
			actualRequest++;
			System.out.println("Actual request: "+actualRequest+" Actual Envío: "+p);
			return imageList.get(p);
		}else {
			System.out.println("Envio final");
			return "";
		}

	}
	
	public void setDataToSend() {
		dataToSend=getImagePart();
	}
	
	
	public String getDataToSend() {
		return dataToSend;
	}



	private void fillImageList() {
		//Method to fill imageList with the bytes of the images
		if(imageEncoded.length()%SUBSTRING_IMAGE_SIZE == 0) {
			setSize(imageEncoded.length()/SUBSTRING_IMAGE_SIZE);
		}else {
			setSize(1+(imageEncoded.length()/SUBSTRING_IMAGE_SIZE));
		}
		
		int initialIndex=0;
		/*The function substring takes from start to (end -1).
		To avoid, not send the position end, we create an auxiliary variable (aux). */
		
		
		for(int i=0; i<getSize();i++) {
			int finalIndex = ((i+1)*SUBSTRING_IMAGE_SIZE)-1;
			String piece = "";
			if(finalIndex <= imageEncoded.length()-1) {
				piece = imageEncoded.substring(initialIndex,finalIndex);
				initialIndex = finalIndex;
			}else {
				//Last iteration
				piece = imageEncoded.substring(initialIndex,imageEncoded.length());
			}	
			imageList.add(piece);
		}
	}




}
