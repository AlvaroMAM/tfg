/*
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
*/
package imageDataObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import org.eclipse.leshan.client.resource.BaseInstanceEnabler;

import org.eclipse.leshan.client.servers.ServerIdentity;

import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.core.response.ReadResponse;

import org.eclipse.leshan.core.response.WriteResponse;
public class ImageObject extends BaseInstanceEnabler {
	//Resource IDs
	private final static int IMAGE_EXECUTE_ID = 27360;
	private final static int IMAGE_ID_ID = 27361;
	private final static int IMAGE_NAME_ID = 27362;
	private final static int IMAGE_TIME_ID = 27363;
	private final static int IMAGE_NUMBER_OF_REQUEST_ID = 27364;
	private final static int IMAGE_ACTUAL_REQUEST_ID = 27365;
	private final static int IMAGE_BYTES_ID = 27366;
	//List of Resources available
	private List<Integer> availableResources = Arrays.asList(IMAGE_EXECUTE_ID, IMAGE_ID_ID, IMAGE_NAME_ID, IMAGE_TIME_ID, IMAGE_NUMBER_OF_REQUEST_ID, IMAGE_ACTUAL_REQUEST_ID, IMAGE_BYTES_ID);

	private final static String IMAGES_PATH = "/images/"; //This variable contains the name of the folder where the images are
	
	
	private List<String> imagesPaths; //This variable saves the paths of the images
	private int actualPath; // This variable is a counter for the list imagesPaths
	private String actualDir; // This variable saves the actual directory where the folder /images is
	private MyImage actualImage;
	
	
	
	private boolean observationEstablished;
	private final Timer timer;
	

	public ImageObject() {
		this.imagesPaths = new ArrayList<String>();
		this.actualDir = System.getProperty("user.dir")+IMAGES_PATH;
		this.fillPaths();
		this.actualPath = 0;
		this.takePhoto(actualDir,imagesPaths.get(actualPath),actualPath);
		this.timer = new Timer("Image-Current-Bytes");
        
		
        
	}
	
	private void fillPaths() {
		//Method to fill paths with the name of the images that are in the folder images
		System.out.println(this.actualDir);
		File file = new File(this.actualDir);
		if(file.exists()) {
			if(file.canRead()) {
				for(String s : file.list()) {
					imagesPaths.add(s);
				}
			}else {
				throw new RuntimeException("File can't be read");
			}
		}else {
			throw new RuntimeException("File doesn't exist");
		}
	}

	private void takePhoto(String path, String imageName, int id) {
		//Simulates the taken of a photo
		this.actualImage = new MyImage(path,imageName,id);
		this.observationEstablished= false;
	}

	
	@Override
	public List<Integer> getAvailableResourceIds(ObjectModel model){
		
		return availableResources;
	}
	
	@Override
	public ObserveResponse observe(ServerIdentity identity, int resourceid) {
		switch (resourceid) {
        case IMAGE_BYTES_ID:
        	
	        	
	        	
	        	if(!isObservationEstablished())	{
	        		
		        		setObservationEstablished(true);
		        		System.out.println("Observation-Confirmed");
		        		
		        		TimerTask task = new TimerTask() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(observationEstablished) {
		                    		actualImage.setDataToSend();
			                		fireResourcesChange(IMAGE_BYTES_ID);
			                		if(actualImage.getDataToSend()=="") {
			                			setObservationEstablished(false);
			                			this.cancel();
			                			getTimer().purge();
			                		}
			                		
		                    	}
							}
						};
		        		
		        		getTimer().schedule(task,1000,2000);
	        		
	        	}
	        	
	    	  ReadResponse readResponse = this.read(identity, resourceid);
	          return new ObserveResponse(readResponse.getCode(), readResponse.getContent(), null, null,
	                 readResponse.getErrorMessage());
        default:
        	return super.observe(identity, resourceid);
        }
		
		
	}
	
	@Override
    public ReadResponse read(ServerIdentity identity, int resourceid) {
        switch (resourceid) {
        case IMAGE_ID_ID:
            return ReadResponse.success(resourceid, actualImage.getId());        
        case IMAGE_NAME_ID:
        	return ReadResponse.success(resourceid, actualImage.getName());      	
        case IMAGE_TIME_ID:
        	return ReadResponse.success(resourceid, actualImage.getDate());
        case IMAGE_NUMBER_OF_REQUEST_ID:
        	return ReadResponse.success(resourceid,actualImage.getSize());
        case IMAGE_ACTUAL_REQUEST_ID:
        	return ReadResponse.success(resourceid,actualImage.getActualRequest());
        case IMAGE_BYTES_ID:
        	return ReadResponse.success(resourceid, actualImage.getDataToSend()); 	
        }
        return ReadResponse.notFound();
    }
	
	
	@Override
    public WriteResponse write(ServerIdentity identity, int resourceid, LwM2mResource value) {

        return WriteResponse.notFound();
    }
	
	
	@Override
    public ExecuteResponse execute(ServerIdentity identity, int resourceid, String params) {
		switch (resourceid) {
		case IMAGE_EXECUTE_ID:
			actualPath++;
			if (actualPath < imagesPaths.size()) {
				takePhoto(actualDir, imagesPaths.get(actualPath), actualPath);
			} else {
				actualPath = 0;
				takePhoto(actualDir, imagesPaths.get(actualPath), actualPath);
			}
			return ExecuteResponse.success();
		}
		return ExecuteResponse.notFound();
	}
	
	protected boolean isObservationEstablished() {
		return observationEstablished;
	}

	protected void setObservationEstablished(boolean observationEstablished) {
		this.observationEstablished = observationEstablished;
	}

	protected Timer getTimer() {
		return timer;
	}

	
}

