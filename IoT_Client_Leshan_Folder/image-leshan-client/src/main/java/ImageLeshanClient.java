/*
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
*/
import java.util.List;
import org.eclipse.leshan.client.californium.LeshanClient;
import org.eclipse.leshan.client.californium.LeshanClientBuilder;
import org.eclipse.leshan.client.object.Security;
import org.eclipse.leshan.client.object.Server;
import org.eclipse.leshan.client.resource.ObjectsInitializer;
import org.eclipse.leshan.core.LwM2m;
import org.eclipse.leshan.core.LwM2mId;
import org.eclipse.leshan.core.model.ObjectLoader;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.model.StaticModel;
import org.eclipse.leshan.core.request.BindingMode;
import deviceObject.MyDevice;
import imageDataObject.ImageObject;

public class ImageLeshanClient {
	private static final int OBJECT_ID=39574;
	//private static final String SERVER_ADDRESS = "coap://127.0.0.1:"+ LwM2m.DEFAULT_COAP_PORT; //On-Local
	private static final String SERVER_ADDRESS = "coap://leshan:"+ LwM2m.DEFAULT_COAP_PORT; //On-Docker
	private static final int SERVER_ID = 123;
	private static final int LIFE_TIME = 300;
	private static final String ENDPOINT_STRING_NAME = "urn:oma:lwm2m:x:"+OBJECT_ID;
	
	public static void main(String[] args) {
		try {
			List<ObjectModel> models = ObjectLoader.loadDefault();
			String [] modelPaths = new String [] {"39574.xml"};
			models.addAll(ObjectLoader.loadDdfResources("/models", modelPaths));
			ObjectsInitializer initializer = new ObjectsInitializer(new StaticModel(models));
			initializer.setInstancesForObject(LwM2mId.SECURITY, Security.noSec(SERVER_ADDRESS, SERVER_ID));
			initializer.setInstancesForObject(LwM2mId.SERVER, new Server(SERVER_ID,LIFE_TIME,BindingMode.U,false));
			initializer.setInstancesForObject(LwM2mId.DEVICE, new MyDevice());
			initializer.setInstancesForObject(OBJECT_ID, new ImageObject());
			LeshanClientBuilder builder = new LeshanClientBuilder(ENDPOINT_STRING_NAME);
			builder.setObjects(initializer.createAll());
			LeshanClient client = builder.build();
			client.start();
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			
		}
	}
}
