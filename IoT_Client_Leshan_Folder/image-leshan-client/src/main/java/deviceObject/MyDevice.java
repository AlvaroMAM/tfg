/*
Apellidos, Nombre: Aparicio Morales, Alvaro Manuel
Grado: Ingenieria del Software
Universidad de Malaga
Título TFG: Analisis de Imagenes en arquitectura cloud-fog con LWM2M
Tutores: Daniel Garrido Marquez
		 Cristian Martin Fernandez
*/


package deviceObject;

import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;


import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.client.servers.ServerIdentity;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.model.ResourceModel.Type;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.request.BindingMode;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;

public class MyDevice extends BaseInstanceEnabler{
	
	//R --> Readable 
	//RW --> Readable and Writeble
	
	
	// Resources IDs
	private static final int MANUFACTURER_ID = 0 ;
	private static final int MODEL_NUMBER_ID = 1 ;
	private static final int SERIAL_NUMBER_ID = 2;
	private static final int FIRMWARE_VERSION_ID = 3;
	private static final int  REBOOT_ID = 4 ;
	private static final int ERROR_CODE_ID = 11;
	private static final int CURRENT_TIME_ID= 13;
	private static final int UTC_OFFSET_ID = 14;
	private static final int TIME_ZONE_ID = 15;
	private static final int SUPPORTED_BINDINGS_MODES_ID = 16;
	private static final int DEVICE_TYPE_ID = 17;
	private static final int SOFTWARE_VERSION_ID = 19;
	private static final int BATTERY_STATUS_ID = 20;
	
	
	private static final List<Integer> availableResources = Arrays.asList(MANUFACTURER_ID, MODEL_NUMBER_ID,SERIAL_NUMBER_ID, 
			FIRMWARE_VERSION_ID, REBOOT_ID, ERROR_CODE_ID,CURRENT_TIME_ID, UTC_OFFSET_ID, TIME_ZONE_ID, SUPPORTED_BINDINGS_MODES_ID, 
			DEVICE_TYPE_ID, SOFTWARE_VERSION_ID, BATTERY_STATUS_ID);
	
	//MANDATORY RESOURCES IN EVERY DEVICE OBJECT
    private static final long ERROR_CODE_0 = 0;
//	private static final long ERROR_CODE_1 = 1;
//	private static final long ERROR_CODE_2 = 2;
//	private static final long ERROR_CODE_3 = 3;
//	private static final long ERROR_CODE_4 = 4;
//	private static final long ERROR_CODE_5 = 5;
//	private static final long ERROR_CODE_6 = 6;
//	private static final long ERROR_CODE_7 = 7;
//	private static final long ERROR_CODE_8 = 8;
    
	//SOME OPTIONAL RESOURCES
	private static final String MANUFACTURER = "University of Malaga ~ ERTIS"; //R
	private static final String SERIAL_NUMBER="124789536"; //R
	private static final String MODEL_NUMBER = "256"; //R
	private static final String FIRMWARE_VERSION = "1.0"; //R
	private static final String DEVICE_TYPE = "Virtual Device"; //R
	private static final String SOFTWARE_VERSION = "1.0.0"; //R
	private static final String SUPPORTED_BINDING=BindingMode.U.toString(); //R
//  BATTERY_STATUS //R
//	private static final Integer BATTERY_STATUS_NORMAL= 0;
//	private static final Integer BATTERY_STATUS_CHARGING= 1;
//	private static final Integer BATTERY_STATUS_CHARGE_COMPLETE = 2;
//	private static final Integer BATTERY_STATUS_DAMAGED = 3;
//	private static final Integer BATTERY_STATUS_LOW_BATTERY = 4;
//	private static final Integer BATTERY_STATUS_NOT_INSTALLED = 5;
	private static final Integer BATTERY_STATUS_UNKNOWN = 6; 
	
	
	private Map <Integer, Long > errorCode;
	private String timeZone; //RW
	private String utcOffSet; //RW
	private Date currentTime; //RW
	
	public MyDevice() {
		this.timeZone = TimeZone.getDefault().getID();
		this.utcOffSet = new SimpleDateFormat("X").format(Calendar.getInstance().getTime());
		this.currentTime = new Date();
		errorCode = new HashMap<Integer, Long>();
		errorCode.put(0,ERROR_CODE_0);
		
		 Timer timer = new Timer("Device-Current Time");
	        timer.schedule(new TimerTask() {
	            @Override
	            public void run() {
	                fireResourcesChange(CURRENT_TIME_ID);
	            }
	        }, 5000, 5000);
		
	}
	

	public String getManufacturer() {
		return 	MANUFACTURER;
	}

	public String getSerialNumber() {
		return SERIAL_NUMBER;
	}

	public String getModelNumber() {
		return MODEL_NUMBER;
	}

	public String getFirmwareVersion() {
		return FIRMWARE_VERSION;
	}

	public String getDeviceType() {
		return DEVICE_TYPE;
	}

	public String getSoftwareVersion() {
		return SOFTWARE_VERSION;
	}

	public String getSupportedBinding() {
		return SUPPORTED_BINDING;
	}

	public Integer getBatteryStatus() {
		return BATTERY_STATUS_UNKNOWN;
	}

	public Map<Integer, Long> getErrorCode() {
			return errorCode;
	}


    public void setErrorCode(Map<Integer, Long> errorCode) {
			this.errorCode = errorCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public Date getCurrentTime() {
		setCurrentTime(new Date());
		return currentTime;
	}
	public String getUtcOffSet() {
		
		return utcOffSet;
	}

	public void setUtcOffSet(String utcOffSet) {
		this.utcOffSet = utcOffSet;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}

	@Override
    public ReadResponse read(ServerIdentity identity, int resourceid) {
        switch (resourceid) {
        case MANUFACTURER_ID:
            return ReadResponse.success(resourceid, getManufacturer() );
            
        case MODEL_NUMBER_ID:
        	return ReadResponse.success(resourceid, getModelNumber() );
        	
        case SERIAL_NUMBER_ID:
        	return ReadResponse.success(resourceid, getSerialNumber() );
        
        case FIRMWARE_VERSION_ID:
        	return ReadResponse.success(resourceid, getFirmwareVersion() );
        
        case CURRENT_TIME_ID:
        	return ReadResponse.success(resourceid, getCurrentTime());
        
        case UTC_OFFSET_ID:
        	return ReadResponse.success(resourceid, getUtcOffSet());
        	
        case TIME_ZONE_ID:
        	return ReadResponse.success(resourceid, getTimeZone());
        	
        case SUPPORTED_BINDINGS_MODES_ID:
        	return ReadResponse.success(resourceid, getSupportedBinding());
        	
        case DEVICE_TYPE_ID:
        	return ReadResponse.success(resourceid, getDeviceType());
        	
        case SOFTWARE_VERSION_ID:
        	return ReadResponse.success(resourceid, getSoftwareVersion());
        	
        case ERROR_CODE_ID:
        	return ReadResponse.success(resourceid,getErrorCode(),Type.INTEGER);
        	
        case BATTERY_STATUS_ID:
        	return ReadResponse.success(resourceid, getBatteryStatus());
        }
        return ReadResponse.notFound();
    }

 


	@Override
    public WriteResponse write(ServerIdentity identity, int resourceid, LwM2mResource value) {
        switch (resourceid) {
        case TIME_ZONE_ID: 
           
        	setTimeZone((String) value.getValue());
        	fireResourcesChange(resourceid);
            return WriteResponse.success();
        case UTC_OFFSET_ID: 
            
        	setUtcOffSet((String) value.getValue());;
        	fireResourcesChange(resourceid);
            return WriteResponse.success();
        case CURRENT_TIME_ID: 
            return WriteResponse.notFound();
        }
        return WriteResponse.notFound();
    }
   

	@Override
    public ExecuteResponse execute(ServerIdentity identity, int resourceid, String params) {
		switch (resourceid) {
		case REBOOT_ID:
			 new Timer("Reboot Lwm2mImageClient").schedule(new TimerTask() {
	                @Override
	                public void run() {
	                    getLwM2mClient().stop(true);
	                    try {
	                        Thread.sleep(100);
	                    } catch (InterruptedException e) {
	                    }
	                    getLwM2mClient().start();
	                    
	                }
	            }, 500);
	            return ExecuteResponse.success();

		default:
			return ExecuteResponse.notFound();
		}
       
    }
	
	@Override
	public List<Integer> getAvailableResourceIds(ObjectModel model){
		return availableResources;
	}
	
}
