REGISTER_URL = 'http://iotregister:80/'  # register a physical device
DB_URL = 'http://mongoapi:80/'  # on docker swarm

REFRESH_TOKEN = '\nLog-in again to refresh your Token.'
ALREADY_REGISTERED = 'User already registered. \nTry to log-in instead.'
